# Physicians.dukehealth.org/ Assessment

__<https://physicians.dukehealth.org/>__

__Screenshot:__

![Screenshot of this website](assets/screenshot.png)

<div id="toc">
<!--TOC-->
</div>

<br>
<hr>

# Accessibility

These checks highlight opportunities to [improve the accessibility of your web app](https://developers.google.com/web/fundamentals/accessibility). Only a subset of accessibility issues can be automatically detected so manual testing is also encouraged.

These items address areas which an automated testing tool cannot cover. Learn more in our guide on [conducting an accessibility review](https://developers.google.com/web/fundamentals/accessibility/how-to-review).

These are global issues. When they are fixed they will likely propagate to sub pages also.


## Lists do not contain only `<li>` elements and script supporting elements (`<script>` and `<template>`). [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships)

Screen readers have a specific way of announcing lists. Ensuring proper list structure aids screen reader output. [Learn more](https://dequeuniversity.com/rules/axe/3.1/list?application=lighthouse).



<h3> The
  List element has direct children that are not allowed inside &lt;li&gt; elements </h3>

__Visual location:__

![ list with invalid children](assets/Annotation_2019-09-27_132031.png)

__HTML location:__

```html
<ul class="dropdown-menu">
```

#### Suggested solution:

This close button in the submenu is a really great idea.  It is super helpful for screen reader and keyboard only users.

Would it be possible to use an `<li>` around the close button instead of a `<div>`?

<details>
<summary>_Additional debugging details_</summary>
Path:<br>
`1,HTML,1,BODY,2,DIV,0,DIV,0,DIV,0,HEADER,0,NAV,0,DIV,2,DIV,0,NAV,1,UL,1,LI,1,UL`<br>
Selector:<br>
`.menu-item--expanded.dropdown.nav-item:nth-child(2) > .dropdown-menu`
</details>

---



## The page has a logical tab order [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: The page has a logical tab order

Description:<br>
Tabbing through the page follows the visual layout. Users cannot focus elements that are offscreen. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#start_with_the_keyboard).

__Success!__

---







## Interactive controls are keyboard focusable [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard)

__I need a human!__ Manual Test: Interactive controls are keyboard focusable

Description:<br>
Custom interactive controls are keyboard focusable and display a focus indicator. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#start_with_the_keyboard).

Pay special attention to menus. The user should be able access the entire menu with the keyboard alone.

__Technically successful, but please consider adding up arrow key functionality too:__

The enter key will expand main menu items. Great work! The down arrow allow expands the menu.  But the up arrow does not collapse it.  It might be confusing. 



---


## Interactive elements indicate their purpose and state  [WCAG 1.4.1](https://www.w3.org/WAI/WCAG21/quickref/#use-of-color) [WCAG 1.3.3](https://www.w3.org/WAI/WCAG21/quickref/#sensory-characteristics)


__I need a human!__ Manual Test: Interactive elements indicate their purpose and state

Description:<br>
Interactive elements, such as links and buttons, should indicate their state and be distinguishable from non-interactive elements. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#interactive_elements_like_links_and_buttons_should_indicate_their_purpose_and_state).

Pay special attention to buttons and links.  For example, links and buttons should have obvious :hover and :focus states that meet WCAG 2.0 AA contrast requirements.

__Success!__

---



## The user's focus is directed to new content added to the page [WCAG 3.2.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#on-input)

__I need a human!__ Manual Test: The user&#39;s focus is directed to new content added to the page

Description:<br>
If new content, such as a dialog, is added to the page, the user&#39;s focus is directed to it. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#start_with_the_keyboard).


### Search drop-down autocomplete keyboard accessibility

Screen readers are not aware of the autocomplete drop down.

__Please watch screen reader demo:__

<video width="640" height="480" controls>
  <source src="assets/phyfinder-head-to-cancer.mp4" type="video/mp4">
</video> 
 
#### Suggested solution:

It seems like hitting the enter key while `:focus` is in the search box should not submit this form.  Allowing a person to submit what turns out to be an invalid choice, won't match the users expectation.

There are a few options on how to remediate it:

__Basic option:__

1- Disable submission when "Select a Specialty or Type to Search" input has `:focus` would indicate they must choose from the list provided.  This would help sighted users as well as screen readers.

2- Consider changing the `placeholder` text from:

"Select a Specialty or Type to Search"

to:

"Filter by Specialty or Type"

The screen reader would concatenate the `aria-label` together with the `placeholder` into a nice sentence that describes the inputs purpose a little more accurately:

"Select a Specialty Filter by Specialty or Type"

3- Handle enter key on input like an invalid form field error handler. The alert would be hidden from sighed users. All this would happen behind the scenes for screen readers.

We can do this by adding an element with the `aria-alert` attribute.

_Pseudo code:_


```
Event: Enter key is hit while element has focus:

  If value in input = dropdown option:
    submit

  If value in input != dropdown option:
    error
```

Throw something like this into a function, and call it when they do not match:

```js
let myAlert = document.createElement("p");
myAlert.setAttribute("class", "sr-only");
myAlert.setAttribute("role", "alert");
let myAlertText = document.createTextNode("Choose a result from your filtered list below.");
myAlert.appendChild(myAlertText);
document.body.appendChild(myAlert);
```

It will announce it to them and they can recover from any issues.

Example from:

<https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Techniques/Using_the_alert_role#Example_2_Dynamically_adding_an_element_with_the_alert_role>


__NOTE!__ Whatever is implemented to remediate the form, it needs to be applied to every instance of the form.  In many cases the `aria-label` attribute, `placeholder` attribute, and `alert` element's error advice would need to match the inputs purpose.

<br>

__OR__

<br>

__Slightly better option:__

The above solution would be valid and make remove the barrier.

Consider the implementation found here: 

<https://www.w3.org/TR/wai-aria-practices/examples/combobox/aria1.1pattern/listbox-combo.html>

The first option from the examples in the link above would be robust.  It would be pretty invasive since the search is already built. And its really complicated to implement correctly.  Complicated does not alway equal better.



<br>

__Or choose your own adventure__

Any other solution that provides enough feedback for the user to interact with the custom control would be fine.



---





## User focus is not accidentally trapped in a region [WCAG 2.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#no-keyboard-trap)

__I need a human!__ Manual Test: User focus is not accidentally trapped in a region

Description:<br>
A user can tab into and out of any control or region without accidentally trapping their focus. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#start_with_the_keyboard).

__Success!__

---





## Custom controls have associated labels [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have associated labels

Description:<br>
Custom interactive controls have associated labels, provided by aria-label or aria-labelledby. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#try_it_with_a_screen_reader).

__Success!__

---



## Visual order on the page follows DOM order [WCAG 1.3.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#meaningful-sequence)

__I need a human!__ Manual Test: Visual order on the page follows DOM order

Description:<br>
DOM order matches the visual order, improving navigation for assistive technology. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#try_it_with_a_screen_reader).

[Textise](https://www.textise.net/) is a neat tool for inspecting the natural order of the website. [View this website on Textise](https://www.textise.net/showText.aspx?strURL=physicians.dukehealth.org/). If nothing has been done in JS to interfere the natural tab order, looking at that or viewing the source will basically follow the order of the markup.

Success!

---






## Offscreen content is hidden from assistive technology [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard) [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: Offscreen content is hidden from assistive technology

Description:<br>
Offscreen content is hidden with display: none or aria-hidden=true. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#try_it_with_a_screen_reader).

Pay special attention to menus. For example, the focus indicator should not be lost while tabbing through a menu.

__Success!__

---



## Headings don't skip levels [WCAG 2.4.6](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#headings-and-labels)

__I need a human!__ Manual Test: Headings don&#39;t skip levels

Description:<br>
Headings are used to create an outline for the page and heading levels are not skipped. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#take_advantage_of_headings_and_landmarks).

__Success!__

---



## HTML5 landmark elements are used to improve navigation [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships) [WCAG 2.4.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#bypass-blocks)

__I need a human!__ Manual Test: HTML5 landmark elements are used to improve navigation

Description:<br>
Landmark elements (&lt;main&gt;, &lt;nav&gt;, etc.) are used to improve the keyboard navigation of the page for assistive technology. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#take_advantage_of_headings_and_landmarks).

__Room for improvement__

Screen reader users love to find Landmarks. Since the site already has a bypass block, the website is already technically compliant. That is why this issue is optional. But people love landmarks. If it is simple to implement please consider adding a `<main>` Landmark, it provides a big payoff!


__Please watch screen reader demo:__

<video width="640" height="480" controls>
  <source src="assets/phyfinder-main.mp4" type="video/mp4">
Your browser does not support the video tag.
</video> 


#### Suggested solution:

The homepage has a `<header>`, `<nav>`, and `<footer>` element. We just need to add a `<main>` around all the content in-between.

The Subpages already have an `<main>` awesome!

Please ensure all regions (nav, main, complementary, footer, etc) share the same `aria-label` on the homepage as the subpages.

---




<hr>
<hr>

This accessibility assessment was generated from a [Chrome Lighthouse](https://developers.google.com/web/tools/lighthouse/) report. The Accessibility portion of Chrome Lighthouse is largely sourced from [Deque's Axe-core](https://github.com/dequelabs/axe-core) engine.

Thorough testing should also include testing with the [WAVE Web Accessibility Evaluation tool](http://wave.webaim.org/).

Accessibility testing also requires a human to determine the validity and seriousness of an issue. Automated tests like this only catch about 40% of accessibility issues. For example, automated tests cannot tell if a menu is keyboard accessible. Please follow the checklist of manual items that require a human to test. 

You can re-run the automated section of this report yourself using the open-source [OpenAssessIt project](https://github.com/OpenAssessItToolkit/openassessit) on GitHub.