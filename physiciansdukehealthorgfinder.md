# Physicians.dukehealth.org/specialist-finder Assessment

__<https://physicians.dukehealth.org/specialist-finder>__

__Screenshot:__

![Screenshot of this website](assets/screenshot-finder.png)

<div id="toc">
<!--TOC-->
</div>

<br>
<hr>

# Accessibility

These checks highlight opportunities to [improve the accessibility of your web app](https://developers.google.com/web/fundamentals/accessibility). Only a subset of accessibility issues can be automatically detected so manual testing is also encouraged.

These items address areas which an automated testing tool cannot cover. Learn more in our guide on [conducting an accessibility review](https://developers.google.com/web/fundamentals/accessibility/how-to-review).


---




## The page has a logical tab order [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: The page has a logical tab order

Description:<br>
Tabbing through the page follows the visual layout. Users cannot focus elements that are offscreen. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#start_with_the_keyboard).

__Success!__

---



## Interactive controls are keyboard focusable [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard)

__I need a human!__ Manual Test: Interactive controls are keyboard focusable

Description:<br>
Custom interactive controls are keyboard focusable and display a focus indicator. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#start_with_the_keyboard).

Pay special attention to menus. The user should be able access the entire menu with the keyboard alone.

__Success!__

---

<br>


## Interactive elements indicate their purpose and state  [WCAG 1.4.1](https://www.w3.org/WAI/WCAG21/quickref/#use-of-color) [WCAG 1.3.3](https://www.w3.org/WAI/WCAG21/quickref/#sensory-characteristics)


__I need a human!__ Manual Test: Interactive elements indicate their purpose and state

Description:<br>
Interactive elements, such as links and buttons, should indicate their state and be distinguishable from non-interactive elements. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#interactive_elements_like_links_and_buttons_should_indicate_their_purpose_and_state).

Pay special attention to buttons and links.  For example, links and buttons should have obvious :hover and :focus states that meet WCAG 2.0 AA contrast requirements.

__Success!__

---

<br>


## The user's focus is directed to new content added to the page [WCAG 3.2.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#on-input)

__I need a human!__ Manual Test: The user&#39;s focus is directed to new content added to the page

Description:<br>
If new content, such as a dialog, is added to the page, the user&#39;s focus is directed to it. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#start_with_the_keyboard).
 

### Screen readers need alerted to search result changes

Screen reader users need alerted when the contents of the search area have changed. It will indicate thier filters were applied successfully.

There are many ways to make ajaxy content more accessible to screen readers.  The least invasive method would be to use the "19 Matching results" text as a way to announce that the results have changed.

__Visual location:__

![button is not descriptive](assets/Annotation_2019-09-27_145203.png)

__HTML location:__

```html
<div class="specialist-counter">
  <div class="count-txt-container" style="display: inline-block;">
    <div class="specialist-count">
      19
    </div>
    <div class="total-speciality" style="opacity: 1;">
      Matching Results
    </div>
  </div>
  <div style="display: none;">
    ...
  </div>
</div>
```

#### Suggested solution:

Mark element as `aria-live="polite"`.  This will tell a screen reader to read this out when the contents of the page changes.

`aria-atomic="true"` ensures that it reads the entire live region and not just what has changed.  without it, it would only read the characters that changed.

For example, if the count goes from "223" to "224", it would only read "4".  With `aria-atomic` it will read "224 Matching Results"

The attribute must be on the page _before it loads_ to work in most screen readers.  

```html
<div class="specialist-counter" aria-live="polite" aria-atomic="true">
  ...
</div>
```

Consider adding it here too:

https://www.dukehealth.org/find-doctors-physicians/?m1=primary_care_provider&m2=internal_medicine

---

<br>

### Search drop-down autocomplete keyboard accessibility

Normalize behavior of this type of dropdown to match main specialist search (after that has been implemented and validated).

__Visual location:__

![autocomplete field insurance](assets/Annotation_insurance.png)

__HTML location:__

```html
<div class="insurance all-filter">
  <label for="insurance-filter-dropdown">Insurance Accepted</label>
  <select name="insurance-filter-dropdown" id="insurance-filter-dropdown" class="">
    ...
  </select>
</div>
```

#### Suggested solution:

Apply whichever solution was used from homepage autocomplete "Select a Specialty or Type to Search" to similar inputs like "Insurance Accepted", so user can use the same design pattern they learned before.

---

<br>

### Popup does not announce itself or shift cursor focus to the activated popup modal.

__Visual location:__

![popup problem](assets/Annotation_popup_problem.png)

Popups can render a page useless to keyboard only users and touch screen users.  Since focus is not managed, the only way a keyboard user can recover from this situation is to reload the page and hope the popup modal does not return. By refreshing the page they will likely lose their place on the page. It is also possible they would lose filters they have applied.

Popup modals are very difficult to make accessible because of the complexity of sending keyboard focus to the modal, adding the ARIA attributes to alert screen reader users that a popup modal has taken control of the page, and allowing the user to dismiss the modal via keyboard, and sending the focus back to where the user was when it happened. 

Frameworks like Bootstrap handle all that complexity pretty well, but creating this functionality from scratch is difficult to do correctly.


#### Suggested solution:

##### Short-term:

__A. Please check if the Escape key dismisses the popup modal.__

If Escape key does not dismiss it, please enable that functionality.

If Escape key functionality already exists, please add a tip for sighted keyboard users.

Currently says:

"_If you have 30 seconds, could you please answer 4 questions to help us improve? Press Next to begin or click the X to close this screen._"

Please let the user know they can also use the Escape key.

"_If you have 30 seconds, could you please answer 4 questions to help us improve? Press Next to begin or to close this popup click the X or hit the Escape key._"

__B. Please try to mitigate the possibility of the popup modal deploying for screen reader users.__
  
The site could try to detect mouse movement with the `onmousemove` listener.  If the site detects mouse movement, then allow the popup to happen. If the site does not detect mouse movement, then suppress the popup.

There are many different screen readers and even more preferences and settings that can interact with the mouse cursor. So this will not work all the time, but would likely catch some scenarios. 

This is not a fix.  Just a way to potentially mitigate the popup problem. 

##### Long-term:

A more accessible popup/modal library will need to replace the current inaccessible one. If we can get the short-term suggestion above on the live website, lets put this replacement idea at the bottom of the list.

---

<br>



## User focus is not accidentally trapped in a region [WCAG 2.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#no-keyboard-trap)

__I need a human!__ Manual Test: User focus is not accidentally trapped in a region

Description:<br>
A user can tab into and out of any control or region without accidentally trapping their focus. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#start_with_the_keyboard).

__Success!__

---

<br>



## Custom controls have associated labels [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have associated labels

Description:<br>
Custom interactive controls have associated labels, provided by aria-label or aria-labelledby. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#try_it_with_a_screen_reader).

__Success!__

---

<br>



## Custom controls have ARIA roles [WCAG 4.1.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#name-role-value)

__I need a human!__ Manual Test: Custom controls have ARIA roles

Description:<br>
Custom interactive controls have appropriate ARIA roles. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#try_it_with_a_screen_reader).

__Success!__

---

<br>




## Visual order on the page follows DOM order [WCAG 1.3.2](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#meaningful-sequence)

__I need a human!__ Manual Test: Visual order on the page follows DOM order

Description:<br>
DOM order matches the visual order, improving navigation for assistive technology. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#try_it_with_a_screen_reader).

[Textise](https://www.textise.net/) is a neat tool for inspecting the natural order of the website. [View this website on Textise](https://www.textise.net/showText.aspx?strURL=physicians.dukehealth.org/specialist-finder). If nothing has been done in JS to interfere the natural tab order, looking at that or viewing the source will basically follow the order of the markup.

__Success!__

---






## Offscreen content is hidden from assistive technology [WCAG 2.1.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#keyboard) [WCAG 2.4.3](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#focus-order)

__I need a human!__ Manual Test: Offscreen content is hidden from assistive technology

Description:<br>
Offscreen content is hidden with display: none or aria-hidden=true. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#try_it_with_a_screen_reader).

Pay special attention to menus. For example, the focus indicator should not be lost while tabbing through a menu.

__Success!__

---





## Headings don't skip levels [WCAG 2.4.6](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#headings-and-labels)

__I need a human!__ Manual Test: Headings don&#39;t skip levels

Description:<br>
Headings are used to create an outline for the page and heading levels are not skipped. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#take_advantage_of_headings_and_landmarks).


### We need to provide additional headings to outline, differenciate, and identify regions on the page


### Site Search

__Visual location:__

![site search](assets/Annotation_site-search.png)

__HTML location:__

```html
<h2 class="visually-hidden global-search-heading">Search form</h2>
```

There are two different types of search on the page, a site search and a specialist search.  We need to provide a way for screen reader users to know which search is which.

#### Suggested solution:

Disambiguate which search is which by calling it "Site Search form" instead.

```html
<h2 class="visually-hidden global-search-heading">Site Search form</h2>
```

----

<br>

### Add heading to "Refine results" section

__Visual location:__

![results needs heading](assets/Annotation-refine-results.png)

__HTML location:__

```html
<div class="refine-result-title">
  <img alt="refine-result-icon" src="/themes/custom/dhrp_bootstrap/images/fas/sliders-black.svg">
  <span>Refine Results</span>
</div>
```

#### Suggested solution:

Add an `<h2>` around refine results

```html
<div class="refine-result-title">
  <img alt="refine-result-icon" src="/themes/custom/dhrp_bootstrap/images/fas/sliders-black.svg">
~  <h2>Refine Results</h2>
</div>
```

Wrap it around any existing element or convert any existing element that is convenient for you. The visual style does not matter.

---

<br>

### Add heading above search result section

__HTML location:__

```html
<div class="doctor-list-container">
  <div class="sort-by-filter">
    ...
  </div>
</div>
<div class="doctor-list">
  <div class="doctor-item">
    ...
  </div>
  ...
  <div class="doctor-item">
    ...
  </div>
</div>
```

#### Suggested solutoin:

Add hidden `<h2>` for "Specialist Search Results" 

![search results area](assets/Annotation-search-results.png)


```html
<div class="doctor-list-container">
  <div class="sort-by-filter">
    ...
  </div>
</div>
+ <h2 class="sr-only">Specialist Search Results</h2>
<div class="doctor-list">
  <div class="doctor-item">
    ...
  </div>
  ...
  <div class="doctor-item">
    ...
  </div>
</div>
```

Consider adding summary of currently applied filters to the Specialist Search Results `<h2>`

__Example:__

Mirror filter selections:

Specialty Cancer, Condition Abdominal wall mass, Age group adult, Located near 27701, Language English, Insurace Accepted Aetna

---



## HTML5 landmark elements are used to improve navigation [WCAG 1.3.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#info-and-relationships) [WCAG 2.4.1](https://www.w3.org/WAI/WCAG21/quickref/?versions=2.0#bypass-blocks)

__I need a human!__ Manual Test: HTML5 landmark elements are used to improve navigation

Description:<br>
Landmark elements (&lt;main&gt;, &lt;nav&gt;, etc.) are used to improve the keyboard navigation of the page for assistive technology. [Learn more](https://developers.google.com/web/fundamentals/accessibility/how-to-review#take_advantage_of_headings_and_landmarks).

__Success!__

---




<hr>
<hr>

This accessibility assessment was generated from a [Chrome Lighthouse](https://developers.google.com/web/tools/lighthouse/) report. The Accessibility portion of Chrome Lighthouse is largely sourced from [Deque's Axe-core](https://github.com/dequelabs/axe-core) engine.

Thorough testing should also include testing with the [WAVE Web Accessibility Evaluation tool](http://wave.webaim.org/).

Accessibility testing also requires a human to determine the validity and seriousness of an issue. Automated tests like this only catch about 40% of accessibility issues. For example, automated tests cannot tell if a menu is keyboard accessible. Please follow the checklist of manual items that require a human to test. 

You can re-run the automated section of this report yourself using the open-source [OpenAssessIt project](https://github.com/OpenAssessItToolkit/openassessit) on GitHub.