# Physicians Re-assessment

https://dhtsws-dhrp8-qa.ocp.dhe.duke.edu/

## Screen reader tests

It is ok with NVDA, but Jaws has problems that make it unusable (JAWS is the most popular screen reader).  

### NVDA not focusing on input filter

For some reason NVDA is not allowing focus on the element.  But it works great in JAWS.

<video width="640" controls>
  <source src="assets/not-focusing--nvda-and-jaws.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>

#### Suggested solution:

I think some of the fixes below might fix this. If not, I'll take it on. 

---

## Form error handling works good, but strips out the search request.

The page removes the search request from the input when it does not find a match.  


<video width="640" controls>
  <source src="assets/deleting-search-resulttscproj--nvda.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>

#### Suggested solution:

Keep the message, but leave the search request and the autocomplete suggestions.


---

##  ARIA attribute issues and JAWS mystery instructions.

The `aria-expanded` true/false state belongs on the `#listboxSpecialityminiFas-listbox-combobox` element below that describes its target.  

__Visual location:__

Please watch video:

<video width="640" controls>
  <source src="assets/jaws-mystery-instructions.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>

Fixing the ARIA stuff in Suggested Solution might fix the instructions that JAWS reads out.

__HTML location:__

_Current HTML:_

```html
<div class="specialty-input" 
  aria-owns="listboxSpecialityminiFas-listbox" 
  aria-haspopup="listbox" 
  id="listboxSpecialityminiFas-listbox-combobox" 
  role="combobox">
  <input type="text" 
    name="specialty-input-box" 
    autocomplete="off" 
    aria-label="Select a Specialty" 
    placeholder="Filter by Specialty or Type" 
    class="specialty-input-box" 
    aria-autocomplete="list" 
    aria-controls="listboxSpecialityminiFas-listbox" id="listboxSpecialityminiFas-input" 
    value="">
  <span aria-label="close specialty" class="specialty-dropdown-icon open" tabindex="0">
  </span>
</div>
```

```html
<div class="specialty-dropdown" style="display: block;">
  <ul class="parent-wrapper listbox" 
  aria-labelledby="listboxSpecialityminiFas-label" 
  role="listbox" 
  id="listboxSpecialityminiFas-listbox">
    <li role="option" 
      class="parent" 
      data-value="parent-94" 
      data-title="Ophthalmology" 
      id="Ophthalmology" 
      aria-selected="false">
        <div class="">Ophthalmology</div>
          <ul class="child-wrapper">
            <li role="option" 
              class="child " 
              data-value="specialty-106"  
              id="Low Vision Rehabilitation Specialist" 
              aria-selected="false" 
              data-title="Low Vision Rehabilitation Specialist">
              <div>
                Low Vision Rehabilitation Specialist
              </div>
            </li>
        </ul>
    </li>
  </ul>
</div>
```

#### Suggested solution:

_New HTML:_

`-` remove `~` change `~` add

```html
<div class="specialty-input" 
  aria-owns="listboxSpecialityminiFas-listbox" 
  aria-haspopup="listbox" 
  id="listboxSpecialityminiFas-listbox-combobox" 
  role="combobox"
+  aria-expanded="true OR false">
  <input type="text" 
    name="specialty-input-box" 
    autocomplete="off" 
    aria-label="Select a Specialty" 
    placeholder="Filter by Specialty or Type" 
    class="specialty-input-box" 
    aria-autocomplete="list" 
    aria-controls="listboxSpecialityminiFas-listbox" id="listboxSpecialityminiFas-input" 
    value="">
  <span aria-label="close specialty" class="specialty-dropdown-icon open" tabindex="0">
  </span>
</div>
```



```html
<div class="specialty-dropdown" style="display: block;">
  <ul class="parent-wrapper listbox" 
~  aria-labelledby="listboxSpecialityminiFas-label" 
  role="listbox" 
  id="listboxSpecialityminiFas-listbox">
    <li role="option" 
      class="parent" 
      data-value="parent-94" 
      data-title="Ophthalmology" 
      id="Ophthalmology" 
      aria-selected="false">
        <div class="">Ophthalmology</div>
          <ul class="child-wrapper">
            <li role="option" 
              class="child " 
              data-value="specialty-106"  
~              id="Low Vision Rehabilitation Specialist" 
              aria-selected="false" 
              data-title="Low Vision Rehabilitation Specialist">
              <div>
                Low Vision Rehabilitation Specialist
              </div>
            </li>
        </ul>
    </li>
  </ul>
</div>
```

Change/issues:

- `aria-labelledby="listboxSpecialityminiFas-label"` is not valid because that `id` does not exist in the DOM (at least, it does not exist at this point in the search process).  Giving it an `aria-label` with a human readable name instead of `aria-labelledby` might work.

- `id="Low Vision Rehabilitation Specialist"` is not valid because `id`s are not allowed to have spaces.  If this `id` is not used, you can remove it. If this `id` is nessessary use `_` underscores.

- `aria-expanded` is always required when a `aria-haspopup` is used.  This attribute will communicate the visual state of the listbox.


__Other potentially helpful resources and tips:__

One of the tricky things about ARIA is that some `role`s require parent or children `role`s to make them work properly.

Since its very difficult to remember every rule, I often check things using the Deque Axe extention because it can find all these ARIA dependencies, like when a parent item is misisng a child ARIA role.

<https://chrome.google.com/webstore/detail/axe-web-accessibility-tes/lhdoppojpmngadmnindnejefpokejbdd?hl=en-US>

The coolest part of the extention is that you can run it at any point and any state the page is in. Meaning you can run it in the middle of any ajaxy process and it will check for ARIA stuff at that exact state.

I have also used this as a reference:

https://www.w3.org/TR/wai-aria-practices/examples/combobox/aria1.1pattern/listbox-combo.html


This is related to Angular, but the principles are the same:

<https://medium.com/@kgotgit/web-accessibility-autocomplete-combobox-with-manual-selection-angular-component-part-2-2f7bc1388b59>





